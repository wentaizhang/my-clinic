# README
This project has been deployed on Heroku.
Heroku URL: https://polimi-hyp-2017-team-10453174.herokuapp.com/

## What is this repository for?

Once you fork this repo, you can completely modify it at your own will
to write your project. You will find the instructions to deploy this
repo directly to your heroku account below.

## How do I get set up to test this locally?

- To run this server locally you should have Nodejs (v7.5.0) installed.

- To install the dependencies:
```
npm install .
```
-   To start the server (see property `scripts` in package.json)
```
npm start
```

## How do I deploy this to Heroku
### Setup heroku and/or login:
- register to Heroku and create an app
- login and click create new app
- Name your app instance
- Download the 'heroku' CLI program (major platforms supported) and login
```
1 » heroku login
2
Enter your Heroku credentials.
3
Email: ***********
4
Password (typing will be hidden):
5
Logged in as ***********
```
- Go in the directory where you store locally your repository
```
1 » cd <myprojectdirectory>
```
- Connect your repo to heroku
```
1 » heroku git:remote -a yourappname
```

### Just type:
```
1 » cd <myprojectdirectory>
2 » git push heroku master
```

The app will restart automatically; you will see messages in the terminal telling you the address of your app.