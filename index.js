const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const sqlDbFactory = require("knex");
const process = require("process");

let sqlDb;

function initSqlDB() {
  /* Locally we should launch the app with TEST=true to use SQLlite:

       > TEST=true node ./index.js

    */
  if (process.env.TEST) {
    sqlDb = sqlDbFactory({
      client: "sqlite3",
      debug: true,
      connection: {
        filename: "./doctorsdb.sqlite"
      }
    });
  } else {
    sqlDb = sqlDbFactory({
      debug: true,
      client: "pg",
      connection: process.env.DATABASE_URL,
      ssl: true
    });
  }
}

function initDb() {
  return sqlDb.schema.hasTable("doctors").then(exists => {
    if (!exists) {
      sqlDb.schema
        .createTable("doctors", table => {
          table.increments();
          table.text("firstname");
          table.text("lastname");
          table.text("image");
          table.text("description");
          table.text("location");
          table.text("service");
          table.text("serv_responsible");
        })
        .createTable("locations", table => {
          table.increments();
          table.text("name");
          table.text("image");
          table.text("info");
          table.text("doctor");
          table.text("service");
        })
        .createTable("services", table => {
          table.increments();
          table.text("name");
          table.text("image");
          table.text("description");
          table.text("location");
          table.text("doctor");
          table.text("doc_responsible");
        })
        .then(() => {
          return Promise.all(
            _.map(doctorsList, d => {
              delete d.id;
              return sqlDb("doctors").insert(d);
            })
            _.map(locationsList, l => {
              delete l.id;
              return sqlDb("locations").insert(l);
            })
            _.map(servicesList, s=> {
              delete s.id;
              return sqlDb("services").insert(s);
            })
          );
        });
    } else {
      return true;
    }
  });
}

const _ = require("lodash");

let serverPort = process.env.PORT || 5000;

let doctorsList = require("./public/server/doctors/doctors-info.json");
let locationsList=require("./public/server/locations/locations-info.json");
let servicesList=require("./public/server/services/services-info.json");

app.use(express.static(__dirname + "/public"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// /* Register REST entry point */
app.get("/doctors", function(req, res) {
  let start = parseInt(_.get(req, "query.start", 0));
  let limit = parseInt(_.get(req, "query.limit", 5));
  let sortby = _.get(req, "query.sort", "none");
  let myQuery = sqlDb("doctors");
  myQuery.limit(limit).offset(start).then(result => {
    res.send(JSON.stringify(result));
  });
});

app.get("/locations", function(req, res) {
  let start = parseInt(_.get(req, "query.start", 0));
  let limit = parseInt(_.get(req, "query.limit", 5));
  let sortby = _.get(req, "query.sort", "none");
  let myQuery = sqlDb("locations");
  myQuery.limit(limit).offset(start).then(result => {
    res.send(JSON.stringify(result));
  });
});

app.get("/services", function(req, res) {
  let start = parseInt(_.get(req, "query.start", 0));
  let limit = parseInt(_.get(req, "query.limit", 5));
  let sortby = _.get(req, "query.sort", "none");
  let myQuery = sqlDb("services");
  myQuery.limit(limit).offset(start).then(result => {
    res.send(JSON.stringify(result));
  });
});


app.post("/doctors", function(req, res) {
  let toappend = {
    name: req.body.name,
    tag: req.body.tag,
    born: req.body.born
  };
  sqlDb("doctors").insert(toappend).then(ids => {
    let id = ids[0];
    res.send(_.merge({ id, toappend }));
  });
});

// app.use(function(req, res) {
//   res.status(400);
//   res.send({ error: "400", title: "404: File Not Found" });
// });

app.set("port", serverPort);

initSqlDB();
initDb();

/* Start the server on port 3000 */
app.listen(serverPort, function() {
  console.log(`Your app is ready at port ${serverPort}`);
});
