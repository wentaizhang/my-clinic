var doctor_fullname;
var doctor_description;
var doctor_services;
var doctor_image;
var doctor_id;
var doctor_list;
var doctor_locations;
var doctor_services;

$(window).ready(function () {

    console.log("Doctor id: "+URL.id);

    doctor_fullname = $("#doctor_fullname");
    doctor_description = $("#doctor_description");
    doctor_image = $("#doctor_image");
    doctor_locations=$("#doctor_locations");
    doctor_services = $("#doctor_services");
    doctor_list = $("#doctor_list");

    $.ajax({
        method: "GET",
        dataType: "json",
        crossDomain: true,
        url: "/server/doctors/doctors-info.json",
        data: {
            id: URL.id
        },
        success: function (response) {
            console.log(response);

            doctor_fullname.text(response[URL.id].firstname+" "+response[URL.id].lastname);
            doctor_description.html(response[URL.id].description);
            doctor_image.attr("src",response[URL.id].image);
            doctor_locations.html(response[URL.id].location);
            doctor_services.html(response[URL.id].service);

        },
        error: function (request, error) {
            console.log(request + ":" + error);
        }
    });

    $.ajax({
        method: "GET",
        dataType: "json",
        crossDomain: true,
        url: "/server/doctors/doctors-info.json",
        success: function (response) {
            console.log(response);
            var el="";
            for(var i=0;i<response.length;i++){
                el+='<li><a href="doctor.html?id='+response[i].id+'">'+response[i].firstname+'</a></li>';
            }
            doctor_list.html(el);
        },
        error: function (request, error) {
            console.log(request + ":" + error);
        }
    });



});


var URL = function () {
  // This function is anonymous, is executed immediately and
  // the return value is assigned to QueryString!
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
        // If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = decodeURIComponent(pair[1]);
        // If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
      query_string[pair[0]] = arr;
        // If third or later entry with this name
    } else {
      query_string[pair[0]].push(decodeURIComponent(pair[1]));
    }
  }
  return query_string;
}();