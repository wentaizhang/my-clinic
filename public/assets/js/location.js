var location_name;
var location_info;
var location_services;
var location_image;
var location_id;
var location_list;
var location_doctors;
var location_services;

$(window).ready(function () {

    console.log("Location id: "+URL.id);

    location_name = $("#location_name");
    location_info = $("#location_info");
    location_image = $("#location_image");
    location_doctors=$("#location_doctors");
    location_services = $("#location_services");
    location_list = $("#location_list");

    $.ajax({
        method: "GET",
        dataType: "json",
        crossDomain: true,
        url: "/server/locations/locations-info.json",
        data: {
            id: URL.id
        },
        success: function (response) {
            console.log(response);

            location_name.text(response[URL.id].name);
            location_info.html(response[URL.id].info);
            location_image.attr("src",response[URL.id].image);
            location_doctors.html(response[URL.id].doctor);
            location_services.html(response[URL.id].service);

        },
        error: function (request, error) {
            console.log(request + ":" + error);
        }
    });

    $.ajax({
        method: "GET",
        dataType: "json",
        crossDomain: true,
        url: "/server/locations/locations-info.json",
        success: function (response) {
            console.log(response);
            var el="";
            for(var i=0;i<response.length;i++){
                el+='<li><a href="location.html?id='+response[i].id+'">'+response[i].name+'</a></li>';
            }
            location_list.html(el);
        },
        error: function (request, error) {
            console.log(request + ":" + error);
        }
    });



});


var URL = function () {
  // This function is anonymous, is executed immediately and
  // the return value is assigned to QueryString!
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
        // If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = decodeURIComponent(pair[1]);
        // If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
      query_string[pair[0]] = arr;
        // If third or later entry with this name
    } else {
      query_string[pair[0]].push(decodeURIComponent(pair[1]));
    }
  }
  return query_string;
}();