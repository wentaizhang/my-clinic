/*--modal--*/

(function($) {

    $.fn.bmdIframe = function(options) {
        var self = this;
        var settings = $.extend({
            classBtn: '.bmd-modalButton',
            defaultW: 1280,
            defaultH: 720
        }, options);

        $(settings.classBtn).on('click', function(e) {
            var allowFullscreen = $(this).attr('data-bmdVideoFullscreen') || false;

            var dataVideo = {
                'src': $(this).attr('data-bmdSrc'),
                'height': $(this).attr('data-bmdHeight') || settings.defaultH,
                'width': $(this).attr('data-bmdWidth') || settings.defaultW
            };

            if (allowFullscreen) dataVideo.allowfullscreen = "";

            // stampiamo i nostri dati nell'iframe
            $(self).find("iframe").attr(dataVideo);
        });

        // se si chiude la modale resettiamo i dati dell'iframe per impedire ad un video di continuare a riprodursi anche quando la modale è chiusa
        this.on('hidden.bs.modal', function() {
            $(this).find('iframe').html("").attr("src", "");
        });

        return this;
    };

})(jQuery);

jQuery(document).ready(function() {
    jQuery("#modal-video").bmdIframe();
});

/*--//modal--*/


/*--doctors--*/

var letters = [];

function getLetterMarkUp(letter) {
    letter = letter.toUpperCase();
    return "<a href='#letter_" + letter + "'>" + letter + "</a>";
}

function getDocLetterMarkUp(letter) {
    letter = letter.toUpperCase();
    return "<h5 id='#letter_" + letter + "'>" + letter + "</h5>";
}

function getDocMarkUp(name, surname, url) {
    return "<a href='" + url + "'>" + surname + " " + name + "</a>";
}

function addDocRow(doc) {
    var name = doc.firstname;
    var surname = doc.lastname;
    var id = doc.id - 1;
    var url = "doctor.html?id=" + id;
    var firstletter = surname.charAt(0);
    var $letters = $("#letters");
    var $doc_list = $("#doc_list");
    if (letters.indexOf(firstletter) === -1) {
        letters.push(firstletter);
        $letters.append(getLetterMarkUp(firstletter));
        $doc_list.append(getDocLetterMarkUp(firstletter));
    }
    $doc_list.append(getDocMarkUp(name, surname, url));
}

let start = 0;
let count = 30;
let sortby = "none";

function updatedoctorsList() {
    fetch(`/doctors?start=${start}&limit=${count}&sort=${sortby}`)
        .then(function(response) {
            return response.json();
        })
        .then(function(data) {
            data.map(addDocRow);
        });
}

/*--//doctors--*/


/*--locations--*/


/*--//locations--*/


function startup() {
    updatedoctorsList();
}

startup();
