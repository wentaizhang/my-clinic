var service_name;
var service_description;
var service_image;
var service_doctors;
var service_locations;
var doc_responsible;
var service_id;
var service_list;


$(window).ready(function () {

    console.log("Service id: "+URL.id);

    service_name = $("#service_name");
    service_description = $("#service_description");
    service_image = $("#service_image");
    service_doctors=$("#service_doctors");
    service_locations = $("#service_locations");
    doc_responsible=$("#doc_responsible");
    service_list = $("#service_list");

    $.ajax({
        method: "GET",
        dataType: "json",
        crossDomain: true,
        url: "/server/services/services-info.json",
        data: {
            id: URL.id
        },
        success: function (response) {
            console.log(response);

            service_name.text(response[URL.id].name);
            service_description.html(response[URL.id].description);
            service_image.attr("src",response[URL.id].image);
            service_doctors.html(response[URL.id].doctor);
            service_locations.html(response[URL.id].location);
            service_responsible.html(response[URL.id].doc_responsible)

        },
        error: function (request, error) {
            console.log(request + ":" + error);
        }
    });

    $.ajax({
        method: "GET",
        dataType: "json",
        crossDomain: true,
        url: "/server/services/services-info.json",
        success: function (response) {
            console.log(response);
            var el="";
            for(var i=0;i<response.length;i++){
                el+='<li><a href="location.html?id='+response[i].id+'">'+response[i].name+'</a></li>';
            }
            service_list.html(el);
        },
        error: function (request, error) {
            console.log(request + ":" + error);
        }
    });



});


var URL = function () {
  // This function is anonymous, is executed immediately and
  // the return value is assigned to QueryString!
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
        // If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = decodeURIComponent(pair[1]);
        // If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
      query_string[pair[0]] = arr;
        // If third or later entry with this name
    } else {
      query_string[pair[0]].push(decodeURIComponent(pair[1]));
    }
  }
  return query_string;
}();